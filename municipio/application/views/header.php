<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="utf-8">
    <title>Gad - Parroquial</title>
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <meta content="" name="keywords">
    <meta content="" name="description">

    <!-- Favicon -->
    <link href="<?= base_url() ?>plantilla/img/favicon.ico" rel="icon">

    <!-- Google Web Fonts -->
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@400;500&family=Roboto:wght@500;700;900&display=swap" rel="stylesheet">

    <!-- Icon Font Stylesheet -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.10.0/css/all.min.css" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.4.1/font/bootstrap-icons.css" rel="stylesheet">

    <!-- Libraries Stylesheet -->
    <link href="<?= base_url() ?>plantilla/lib/animate/animate.min.css" rel="stylesheet">
    <link href="<?= base_url() ?>plantilla/lib/owlcarousel/assets/owl.carousel.min.css" rel="stylesheet">
    <link href="<?= base_url() ?>plantilla/lib/lightbox/css/lightbox.min.css" rel="stylesheet">

    <!-- Customized Bootstrap Stylesheet -->
    <link href="<?= base_url() ?>plantilla/css/bootstrap.min.css" rel="stylesheet">

    <!-- Template Stylesheet -->
    <link href="<?= base_url() ?>plantilla/css/style.css" rel="stylesheet">
<link href="<?= base_url() ?>plantilla/css/style.css" rel="stylesheet">
    src="https://code.jquery.com/jquery-3.6.0.js"
  integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk="
  crossorigin="anonymous"></script>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/css/all.min.css" integrity="sha512-KfkfwYDsLkIlwQp6LFnl8zNdLGxu9YAA1QvwINks4PhcElQSvqcyVLLD9aMhXd13uQjoXtEKNosOWaZqXgel0g==" crossorigin="anonymous" referrerpolicy="no-referrer" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/izitoast/1.4.0/js/iziToast.min.js" integrity="sha512-Zq9o+E00xhhR/7vJ49mxFNJ0KQw1E1TMWkPTxrWcnpfEFDEXgUiwJHIKit93EW/XxE31HSI5GEOW06G6BF1AtA==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/izitoast/1.4.0/css/iziToast.css" integrity="sha512-DIW4FkYTOxjCqRt7oS9BFO+nVOwDL4bzukDyDtMO7crjUZhwpyrWBFroq+IqRe6VnJkTpRAS6nhDvf0w+wHmxg==" crossorigin="anonymous" referrerpolicy="no-referrer" />

  <script type="text/javascript">
  jQuery.validator.addMethod("letras", function(value, element) {
 //return this.optional(element) || /^[a-z]+$/i.test(value);
return this.optional(element) || /^[A-Za-zÁÉÍÑÓÚáé íñó]*$/.test(value);

}, "Este campo solo acepta letras");
  </script>
</head>

<body>
    <!-- Spinner Start -->

    <!-- Spinner End -->


    <!-- Topbar Start -->
    <div class="container-fluid bg-dark p-0">
        <div class="row gx-0 d-none d-lg-flex">
            <div class="col-lg-7 px-5 text-start">
                <div class="h-100 d-inline-flex align-items-center me-4">
                    <small class="fa fa-map-marker-alt text-primary me-2"></small>
                    <small>123 Street, Latacunga, Cotopaxi</small>
                </div>
                <div class="h-100 d-inline-flex align-items-center">
                    <small class="far fa-clock text-primary me-2"></small>
                    <small>Lunes - Viernes : 09.00 AM - 09.00 PM</small>
                </div>
            </div>
            <div class="col-lg-5 px-5 text-end">
                <div class="h-100 d-inline-flex align-items-center me-4">
                    <small class="fa fa-phone-alt text-primary me-2"></small>
                    <small>+012 345 6789</small>
                </div>
                <div class="h-100 d-inline-flex align-items-center mx-n2">
                    <a class="btn btn-square btn-link rounded-0 border-0 border-end border-secondary" href=""><i class="fab fa-facebook-f"></i></a>
                    <a class="btn btn-square btn-link rounded-0 border-0 border-end border-secondary" href=""><i class="fab fa-twitter"></i></a>
                    <a class="btn btn-square btn-link rounded-0 border-0 border-end border-secondary" href=""><i class="fab fa-linkedin-in"></i></a>
                    <a class="btn btn-square btn-link rounded-0" href=""><i class="fab fa-instagram"></i></a>
                </div>
            </div>
        </div>
    </div>
    <!-- Topbar End -->


    <!-- Navbar Start -->
    <nav class="navbar navbar-expand-lg bg-white navbar-light sticky-top p-0">
        <a href="index.html" class="navbar-brand d-flex align-items-center border-end px-4 px-lg-5">
            <h2 class="m-0 text-primary">GAD</h2>
        </a>
        <button type="button" class="navbar-toggler me-4" data-bs-toggle="collapse" data-bs-target="#navbarCollapse">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarCollapse">
            <div class="navbar-nav ms-auto p-4 p-lg-0">

  
              <a class="nav-link" href="<?php echo site_url(); ?>"Inicio">
                                <i class="typcn typcn-device-desktop menu-icon"></i>
                                <span class="menu-title">Inicio</span>



                <a href="service.html" class="nav-item nav-link">Agenda eventos</a>
                <a href="service.html" class="nav-item nav-link">Agenda cultural</a>
                <a href="project.html" class="nav-item nav-link">consulta de impuestos</a>
                <a href="project.html" class="nav-item nav-link">turnos movalidad</a>


                <div class="nav-item dropdown">
                    <a href="#" class="nav-link dropdown-toggle" data-bs-toggle="dropdown">Mas</a>
                    <div class="dropdown-menu bg-light m-0">
                      <a class="nav-link" href="<?php echo site_url(); ?>/pagos/index">Pagos en linea</a>
                                              <i class="typcn typcn-device-desktop menu-icon"></i>
                                              <span class="menu-title"></span>
                                                  </a>

                      <a class="nav-link" href="<?php echo site_url(); ?>/pagos/cliente">Consegal</a>
                                              <i class="typcn typcn-device-desktop menu-icon"></i>
                                              <span class="menu-title"></span>
                                                  </a>

                        <a class="nav-link" href="<?php echo site_url(); ?>/pagos/area">Areas financiera</a>
                                <i class="typcn typcn-device-desktop menu-icon"></i>
                                    <span class="menu-title"></span>
                                                  </a>

                                                  <a class="nav-link" href="<?php echo site_url(); ?>/pagos/ministro">Areas administrativas</a>
                                                          <i class="typcn typcn-device-desktop menu-icon"></i>
                                                              <span class="menu-title"></span>
                                                                            </a>


                          <a href="testimonial.html" class="dropdown-item">Comunicacion social</a>

                        <a href="404.html" class="dropdown-item">404 Page</a>
                    </div>
                </div>
                <a class="nav-link" href="<?php echo site_url(); ?>/pagos/contacto">Contactanos</a>
                        <i class="typcn typcn-device-desktop menu-icon"></i>
                            <span class="menu-title"></span>
                                          </a>
            </div>

        </div>
    </nav>
    <!-- Navbar End -->


    <!-- Carousel Start -->
    <div class="container-fluid p-0 pb-5 wow fadeIn" data-wow-delay="0.1s">
        <div class="owl-carousel header-carousel position-relative">
            <div class="owl-carousel-item position-relative" data-dot="<img src='plantilla/img/carousel-1.jpg'>">
                <img class="img-fluid" src="plantilla/img/carousel-1.jpg" alt="">
                <div class="owl-carousel-inner">
                    <div class="container">
                        <div class="row justify-content-start">
                            <div class="col-10 col-lg-8">
                                <h1 class="display-2 text-white animated slideInDown">Obras en progreso para un mejor futuro</h1>
                                <p class="fs-5 fw-medium text-white mb-4 pb-3"> </p>
                                <a href="" class="btn btn-primary rounded-pill py-3 px-5 animated slideInLeft">Read More</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="owl-carousel-item position-relative" data-dot="<img src='plantilla/img/carousel-2.jpg'>">
                <img class="img-fluid" src="plantilla/img/carousel-2.jpg" alt="">
                <div class="owl-carousel-inner">
                    <div class="container">
                        <div class="row justify-content-start">
                            <div class="col-10 col-lg-8">
                                <h1 class="display-2 text-white animated slideInDown">Implementacion de paneles solares</h1>
                                <p class="fs-5 fw-medium text-white mb-4 pb-3"></p>
                                <a href="" class="btn btn-primary rounded-pill py-3 px-5 animated slideInLeft">Read More</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
			  <div class="owl-carousel-item position-relative" data-dot="<img src='plantilla/img/carousel-3.jpg'>">
                <img class="img-fluid" src="plantilla/img/carousel-3.jpg" alt="">
                <div class="owl-carousel-inner">
                    <div class="container">
                        <div class="row justify-content-start">
                            <div class="col-10 col-lg-8">
                                <h1 class="display-2 text-white animated slideInDown"></h1>
                                <p class="fs-5 fw-medium text-white mb-4 pb-3"></p>
                                <a href="" class="btn btn-primary rounded-pill py-3 px-5 animated slideInLeft"></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Carousel End -->

