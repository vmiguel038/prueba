
    <!-- Footer Start -->
    <div class="container-fluid bg-dark text-body footer mt-5 pt-5 wow fadeIn" data-wow-delay="0.1s">
        <div class="container py-5">
            <div class="row g-5">
                <div class="col-lg-3 col-md-6">
                    <h5 class="text-white mb-4">Dirección</h5>
                    <p class="mb-2"><i class="fa fa-map-marker-alt me-3"></i>123 Street, Latacunga, Cotopaxi</p>
                    <p class="mb-2"><i class="fa fa-phone-alt me-3"></i>+012 345 67890</p>
                    <p class="mb-2"><i class="fa fa-envelope me-3"></i>Gad.latacunga@gmail.com</p>
                    <div class="d-flex pt-2">
                        <a class="btn btn-square btn-outline-light btn-social" href=""><i class="fab fa-twitter"></i></a>
                        <a class="btn btn-square btn-outline-light btn-social" href=""><i class="fab fa-facebook-f"></i></a>
                        <a class="btn btn-square btn-outline-light btn-social" href=""><i class="fab fa-youtube"></i></a>
                        <a class="btn btn-square btn-outline-light btn-social" href=""><i class="fab fa-linkedin-in"></i></a>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <h5 class="text-white mb-4">enlaces rápidos</h5>
                    <a class="btn btn-link" href="">Sobre nosotros</a>
                    <a class="btn btn-link" href="">Contactenos</a>
                    <a class="btn btn-link" href="">Nuestros servicios</a>
                    <a class="btn btn-link" href="">Tèrminos y condiciones</a>

                </div>
                <div class="col-lg-3 col-md-6">
                    <h5 class="text-white mb-4">Galería de proyectos</h5>
                    <div class="row g-2">
                        <div class="col-4">
                            <img class="img-fluid rounded" src="plantilla/img/gallery-1.jpg" alt="">
                        </div>
                        <div class="col-4">
                            <img class="img-fluid rounded" src="plantilla/img/gallery-2.jpg" alt="">
                        </div>
                        <div class="col-4">
                            <img class="img-fluid rounded" src="plantilla/img/gallery-3.jpg" alt="">
                        </div>
                        <div class="col-4">
                            <img class="img-fluid rounded" src="plantilla/img/gallery-4.jpg" alt="">
                        </div>
                        <div class="col-4">
                            <img class="img-fluid rounded" src="plantilla/img/gallery-5.jpg" alt="">
                        </div>
                        <div class="col-4">
                            <img class="img-fluid rounded" src="plantilla/img/gallery-6.jpg" alt="">
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">

                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="copyright">
                <div class="row">
                    <div class="col-md-6 text-center text-md-start mb-3 mb-md-0">
                        &copy; <a href="#">Gap-Parroquial</a>,  todos los derechos reservados..
                    </div>
                    <div class="col-md-6 text-center text-md-end">
                        <!--/*** This template is free as long as you keep the footer author’s credit link/attribution link/backlink. If you'd like to use the template without the footer author’s credit link/attribution link/backlink, you can purchase the Credit Removal License from "https://htmlcodex.com/credit-removal". Thank you for your support. ***/-->
                        Diseñador por  <a href="">Sistemas de informaciòn</a>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Footer End -->


    <!-- Back to Top -->
    <a href="#" class="btn btn-lg btn-primary btn-lg-square rounded-circle back-to-top"><i class="bi bi-arrow-up"></i></a>


    <!-- JavaScript Libraries -->
    <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0/dist/js/bootstrap.bundle.min.js"></script>
    <script src="plantilla/lib/wow/wow.min.js"></script>
    <script src="plantilla/lib/easing/easing.min.js"></script>
    <script src="plantilla/lib/waypoints/waypoints.min.js"></script>
    <script src="plantilla/lib/counterup/counterup.min.js"></script>
    <script src="plantilla/lib/owlcarousel/owl.carousel.min.js"></script>
    <script src="plantilla/lib/isotope/isotope.pkgd.min.js"></script>
    <script src="plantilla/lib/lightbox/js/lightbox.min.js"></script>

    <!-- Template Javascript -->
    <script src="plantilla/js/main.js"></script>
</body>

</html>

<?php if ($this->session->flashdata("confirmacion")): ?>
  <script type="text/javascript">
  iziToast.success({
    title: 'CONFIRMACIÓN',
    message: 'Successfully inserted record!',
    position: 'topRight',
  });
  </script>
<?php endif; ?>

<?php if ($this->session->flashdata("error")): ?>
  <script type="text/javascript">
  iziToast.danger({
    title: 'ADVERTENCIA',
    message: 'Successfully inserted record!',
    position: 'topRight',
  });
  </script>
<?php endif; ?>

<?php if ($this->session->flashdata("seleccion")): ?>
  <script type="text/javascript">
  iziToast.danger({
    title: 'SELECCIÓN',
    message: 'Successfully inserted record!',
    position: 'topRight',
  });
  </script>
<?php endif; ?>

<style media="screen">
  .error {
    color: red;
    font-size: 16px;
  }
  input.error, select.error{
  border:2px solid red;
    }
</style>

</body>

</html>
