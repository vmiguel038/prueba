<?php
class Pagos extends CI_Controller{
public function __construct(){
   parent:: __construct();
   $this->load->model("Pago");

}

public function index(){
$data["listadoPagos"]=$this->Pago->consultarTodos();
$this->load->view("header");
$this->load->view("pagos/index",$data);
$this->load->view("footer");

}

public function nuevo(){
$this->load->view("header");
$this->load->view("pagos/nuevo");
$this->load->view("footer");

}





public function guardarPago(){
  $datosNuevosPago=array(
  "identificacion_pag"=>$this->input->post("identificacion_pag"),
  "titulo_pag"=>$this->input->post("titulo_pag"),
  "interes_pag"=>$this->input->post("interes_pag"),
    "recargo_pag"=>$this->input->post("recargo_pag"),
  "estado_pag"=>$this->input->post("estado_pag"),
  );

  if ($this->Pago->insertar($datosNuevosPago)) {
    //echo "inserccion exitosa";
      redirect("pagos/index");
  }else{
    echo "error al insertar";

  }

}


    public function procesarEliminacion($id_pag){
    if($this->Pago->eliminar($id_pag)){
      redirect("pagos/index");
    }else{
    echo "Error al eliminar";

    }
}


}//llave cierre de la clase

 ?>

