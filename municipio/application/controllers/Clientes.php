<?php
class Clientes extends CI_Controller{
public function __construct(){
   parent:: __construct();
   $this->load->model("Cliente");

}

public function index(){

$this->load->view("header");
$this->load->view("clientes/index");
$this->load->view("footer");

}

public function nuevo(){
$this->load->view("header");
$this->load->view("clientes/nuevo");
$this->load->view("footer");

}


public function consegal(){
    $data["listadoclientes"]=$this->Cliente->consultarTodos();
$this->load->view("header");
$this->load->view("pagos/cliente"$data);
$this->load->view("footer");

}




public function guardarCliente(){
  $datosNuevosCliente=array(
  "identificacion_cli"=>$this->input->post("identificacion_cli"),
  "apellido_cli"=>$this->input->post("apellido_cli"),
  "nombre_cli"=>$this->input->post("nombre_cli"),
  "telefono_cli"=>$this->input->post("telefono_cli"),
  "direccion_cli"=>$this->input->post("direccion_cli"),
  "email_cli"=>$this->input->post("email_cli"),
  "estado_cli"=>$this->input->post("estado_cli"),
  );

  if ($this->Cliente->insertar($datosNuevosCliente)) {
    //echo "inserccion exitosa";
    redirect("pagos/cliente");
  }else{
    echo "error al insertar";

  }

  }

    public function procesarEliminacion($id_cli){
    if($this->Cliente->eliminar($id_cli)){
      redirect("clientes/index");
    }else{
    echo "Error al eliminar";

    }
}


}//llave cierre de la clase

 ?>

