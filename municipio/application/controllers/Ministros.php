
<?php
class Ministros extends CI_Controller{
public function __construct(){
   parent:: __construct();
   $this->load->model("Ministro");

}

public function index(){

$this->load->view("header");
$this->load->view("areas/index");
$this->load->view("footer");

}

public function nuevo(){
$this->load->view("header");
$this->load->view("areas/nuevo");
$this->load->view("footer");

}




public function area(){
  $data["listadoAreas"]=$this->Area->consultarTodos();
$this->load->view("header");
$this->load->view("pagos/area",$data);
$this->load->view("footer");

}

public function ministro(){
  $data["listadoMinistros"]=$this->Ministro->consultarTodos();
$this->load->view("header");
$this->load->view("pagos/ministro",$data);
$this->load->view("footer");

}






public function guardarMinistro(){
  $datosNuevosMinistro=array(
    "identificacion_min"=>$this->input->post("identificacion_min"),
  "nombre_min"=>$this->input->post("nombre_min"),
  "telefono_min"=>$this->input->post("telefono_min"),
    "email_min"=>$this->input->post("email_min"),
  "estado_min"=>$this->input->post("estado_min"),

  );

  if ($this->Ministro->insertar($datosNuevosMinistro)) {
    echo "inserccion exitosa";
  }else{
    echo "error al insertar";

  }

}


}//llave cierre de la clase

 ?>
